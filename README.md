TimeCard Control

## Demo
You can test a fully working live demo at ~~https://igorantun.com/chat~~ *(demo currently offline)*
- Type `/help` to get a list of the available chat commands

---

## Features



####There are 3 admin levels:
- **Helper:** Can delete chat messages
- **Moderator:** The above plus the ability to kick and ban users
- **Administrator:** All the above plus send global alerts and promote/demote users

---

## Setup
Clone this repo to your desktop and run `npm install` to install all the dependencies.

---

## Usage
After you clone this repo to your desktop, go to its root directory and run `npm install` to install its dependencies.

Once the dependencies are installed, you can run  `npm start` to start the application. You will then be able to access it at localhost:3000

To give yourself administrator permissions on the chat, you will have to type `/role [your-name]` in the app console.

---

## License
>This project is licensed under the terms of the **AGPL-3.0** license.
